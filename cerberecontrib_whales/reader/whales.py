from dateutil import parser
from pathlib import Path
import re
import typing as T

import numpy as np
import xarray as xr
from cerbere.reader.basereader import BaseReader

CCI_TIME_UNITS = "seconds since 2000-01-01 00:00:00.0"
TUM_TIME_UNITS = "days since 2000-01-01 12:00:00"


class CCIWHALES(BaseReader):
    pattern: str = re.compile(r"^.*CS_OFFL_SIR_LRM_1B_[0-9]{8}T[0-9]{6}_[0-9]{8}T[0-9]{6}_D001.nc$")
    engine: str = "netcdf4"
    description: str = "WHALES Altimeter Retracker output format, CCI version"
    url: str = "https://gitlab.ifremer.fr/cerbere/cerberecontrib-whales"
    time_units: str = CCI_TIME_UNITS

    @classmethod
    def postprocess(cls, ds: xr.Dataset, **kwargs):
        ds = ds.rename_vars(
            {
                'time_20hz': 'time',
                'lat_20hz': 'lat',
                'lon_20hz': 'lon',
            }
        )

        if 'records' in ds.dims:
            if ds.sizes['records'] == 1:
                ds = ds.squeeze('records')
            else:
                # squeeze records dimension
                ds = ds.rename_dims({'time': 'time_'})
                ds = ds.stack(time_dim=('time_', 'records'))
                ds = ds.drop(['time_dim'])
                ds = ds.swap_dims({'time_dim': 'time'})

        # fix invalid values
        for v in ['lat', 'lon']:
            ds[v][:] = np.ma.masked_greater_equal(ds[v], 2147483647)
        ds = ds.set_coords('time')
        ds['time'] = xr.DataArray(
            dims=('time',),
            data=np.ma.masked_greater(ds['time'], 1e19),
            attrs=ds.time.attrs)

        ds['time'].attrs['units'] = cls.time_units
        ds = xr.decode_cf(ds)

        # longitudes between -180/180
        lon = ds.lon.to_masked_array()
        lon[lon > 180] -= 360
        ds['lon'][:] = lon

        # attributes
        ds['time'].attrs['axis'] = 'T'
        ds['lat'].attrs['axis'] = 'Y'
        ds['lon'].attrs['axis'] = 'X'
        ds['lat'].attrs['units'] = 'degree_north'
        ds['lat'].attrs['units'] = 'degree_east'

        ds.attrs['time_coverage_start'] = ds.cb.time_coverage_start
        ds.attrs['time_coverage_end'] = ds.cb.time_coverage_end

        return ds


class TUMWHALES(CCIWHALES):
    pattern: str = re.compile(r"^.*JA3_GPS_.*_[0-9]{8}_[0-9]{6}_[0-9]{8}_[0-9]{6}.nc$")
    engine: str = "netcdf4"
    description: str = "WHALES Altimeter Retracker output format, TUM version"
    url: str = "https://gitlab.ifremer.fr/cerbere/cerberecontrib-whales"
    time_units: str = TUM_TIME_UNITS

    @classmethod
    def postprocess(cls, ds: xr.Dataset, **kwargs):
        ds = ds.rename_vars(
            {
                'glat.00': 'lat_20hz',
                'glon.00': 'lon_20hz',
                'swh.07': 'swh_WHALES_20hz',
                'stdalt.07': 'swh_WHALES_fitting_error_20hz',
                'swhinstrcorr.07': 'swh_WHALES_instr_corr_20hz',
            }
        )
        if 'sigma0.07' in ds:
            ds = ds.rename_vars({'sigma0.07': 'sigma0_WHALES_20hz'})

        ds['time_20hz'] = xr.DataArray(
            dims=('time',),
            data=np.ma.masked_equal(ds['jday.00'], 0),
        )
        ds['time_20hz'].attrs['units'] = cls.time_units
        ds = ds.drop_vars(['jday.00'])

        ds = super(cls, TUMWHALES).postprocess(ds)

        return ds

