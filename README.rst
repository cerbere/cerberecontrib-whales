==============================================
Cerbere extensions for WHALES retracker format
==============================================

Classes derived from Cerbere base class :class:`~cerbere.dataset.dataset.Dataset` 
to read output files of TUM/WHALES altimeter retracker.

Tested products
===============

 * WHALES output from ESA ENVISAT version 3 SGDR products (:class:`WHALESNCDataset`, :class:`Trajectory`)
 * WHALES output from ESA CryoSat2 version D LRM L1B products (:class:`WHALESNCDataset`, :class:`Trajectory`)
 * WHALES output from ESA SARAL SGDR version T products (:class:`SARALvTWHALESNCDataset`, :class:`Trajectory`)
 * WHALES output from JASON products, in TUM's format flavour (:class:`JASONWHALESNCDataset`, :class:`Trajectory`)


Installation
============

Requires ``cerbere>=2.0.0``

Test data
=========

Sample products for each class can be found at:

ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/

They are used by the unitary tests in ``tests`` directory.


Usage example
=============

.. code-block:: python

  import cerbere

  # open file
  dst = cerbere.open_dataset(
      'CS_LTA__SIR_LRM_1B_20100719T070328_20100719T070439_D001.nc',
      'WHALESNCDataset'
  )

  print(dst.get_times())
  print(dst.get_lon())
  print(dst.fieldnames)
  print(dst.get_values('swh_WHALES_20hz'))
