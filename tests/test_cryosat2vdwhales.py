from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
        test_data_dir,
        'CRYOSAT2vD',
        'CS_OFFL_SIR_LRM_1B_20200708T031000_20200708T032124_D001.nc',
    )


@pytest.fixture(scope='module')
def reader():
    return 'WHALES'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
