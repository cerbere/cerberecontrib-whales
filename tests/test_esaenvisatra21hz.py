from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *

TEST_FILE = Path(
    os.environ['CERBERE_TESTDATA_REPOSITORY'],
    'ENVISATv3',
    'ENV_RA_2_MWS____20100101T000047_20100101T005104_20170905T025157_3017_085_0691____PAC_R_NT_003.nc'
)


@pytest.fixture(scope='module')
def test_file():
    return TEST_FILE


@pytest.fixture(scope='module')
def reader():
    return 'ESAEnvisatRA21Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
