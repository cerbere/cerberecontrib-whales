from pathlib import Path

import pytest

from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
        test_data_dir,
        'JASON3',
        'JA3_GPS_2PdP115_198_20190331_121505_20190331_131118.nc'
    )


@pytest.fixture(scope='module')
def reader():
    return 'TUMWHALES'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
