from cerbere.feature.ctrajectory import Trajectory
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
        test_data_dir,
        'JASON2',
        'JA2_GPS_2PdP092_017_20110101_000523_20110101_010136.nc'
    )


@pytest.fixture(scope='module')
def reader():
    return 'JASON2WHALES'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
